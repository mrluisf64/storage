import ItemController from "../controllers/item.controller";
import { Router } from "express";
import { TokenValidation } from "../libs/verifyToken";
import { VerifyIsAdmin } from "../libs/verifyIsAdmin";

const router: Router = Router();

router.get("/", TokenValidation, ItemController.index);
router.post("/", [TokenValidation, VerifyIsAdmin], ItemController.create);
router.put("/", [TokenValidation, VerifyIsAdmin], ItemController.update);
router.delete(
  "/:itemId",
  [TokenValidation, VerifyIsAdmin],
  ItemController.destroy
);

export default router;
