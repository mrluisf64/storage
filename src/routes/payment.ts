import paymentController from "../controllers/payment.controller";
import { Router } from "express";
import { TokenValidation } from "../libs/verifyToken";

const router: Router = Router();

router.post("/recharge", TokenValidation, paymentController.recharge);
router.get("/consult", TokenValidation, paymentController.consult);
router.post("/buy", TokenValidation, paymentController.buy);

export default router;
