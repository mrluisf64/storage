import app from './app'

function main (): void {
  app.listen(app.get('port'), () => {
    console.log('server run!')
  })
}

main()
