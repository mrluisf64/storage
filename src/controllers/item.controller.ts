import Item from "../database/entities/item.entity";
import { Request, Response } from "express";
import { getRepository } from "typeorm";

class ItemController {
  async index(req: Request, res: Response) {
    const listTasks = await getRepository(Item).find();

    res.send(listTasks);
  }

  async create(req: Request, res: Response) {
    const repository = getRepository(Item);

    const item = new Item();
    item.name = req.body.name;
    item.description = req.body.description;
    item.price = req.body.price;
    item.category = req.body.category;
    item.stock = req.body.stock;

    const itemSaved = await repository.save(item);

    res.json({
      body: itemSaved,
      status: 200,
    });
  }

  async update(req: Request, res: Response) {
    const repository = getRepository(Item);
    const itemUpdated = await repository.findOne(req.body.itemId);
    if (!itemUpdated) return res.status(400).json("Item no exists");
    itemUpdated.name = req.body.name ? req.body.name : itemUpdated.name;
    itemUpdated.description = req.body.description
      ? req.body.description
      : itemUpdated.description;
    itemUpdated.price = req.body.price ? req.body.price : itemUpdated.price;
    itemUpdated.category = req.body.category
      ? req.body.category
      : itemUpdated.category;
    itemUpdated.stock = req.body.stock ? req.body.stock : itemUpdated.stock;
    const itemSaved: Item = await repository.save(itemUpdated);

    res.json({
      body: itemSaved,
      status: 200,
    });
  }

  async destroy(req: Request, res: Response) {
    const repository = getRepository(Item);
    const itemDeleted = await repository.findOne(req.body.itemId);
    if (!itemDeleted) return res.status(400).json("Item no exists");

    await repository.delete(itemDeleted);
    res.status(200).json({
      body: "OK",
      status: 200,
    });
  }
}

export default new ItemController();
