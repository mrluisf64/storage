import User from "../database/entities/user.entity";
import { Response, Request } from "express";
import { getRepository } from "typeorm";
import Wallet from "../database/entities/wallet.entity";
import Transaction from "../database/entities/transaction.entity";
import Item from "../database/entities/item.entity";

async function generateTrans(
  title: string,
  description: string,
  value: string | number | null,
  user: User
): Promise<Transaction> {
  const transRepo = getRepository(Transaction);

  const trans = new Transaction();
  trans.title = title;
  trans.description = description;
  trans.mount = value;
  trans.user = user;

  return transRepo.save(trans);
}

class PaymentController {
  async recharge(req: Request, res: Response) {
    if (!req.body.value) return res.status(400).send("Ingrese un valor");
    if (req.body.value < 0)
      return res.status(400).send("Ingrese un valor correcto");

    const userRepo = getRepository(User);
    const walletRepo = getRepository(Wallet);
    const value = parseFloat(req.body.value);

    const user = await userRepo.findOne(req.userId, {
      relations: ["wallet"],
    });
    if (!user) return res.status(400).send("User not found");

    await walletRepo.update(
      { id: user.wallet.id },
      {
        balance: user.wallet.balance + value,
      }
    );

    await generateTrans(
      "Recarga de saldo",
      "Recargar saldo en billetera",
      value,
      user
    );

    res.status(200).send("recharge sucessfully");
  }

  async consult(req: Request, res: Response) {
    const userRepo = getRepository(User);

    const user = await userRepo.findOne(req.userId, {
      relations: ["wallet"],
    });
    if (!user) return res.status(400).send("User not found");

    await generateTrans(
      "Consulta de saldo",
      "Consultar saldo en billetera",
      null,
      user
    );

    res.status(200).send({
      message: "consult successfully",
      body: user.wallet,
    });
  }

  async buy(req: Request, res: Response) {
    if (!req.body.itemId) return res.status(400).send("Falta el producto");

    const userRepo = getRepository(User);
    const itemRepo = getRepository(Item);
    const walletRepo = getRepository(Wallet);
    const value = parseFloat(req.body.value);

    const user = await userRepo.findOne(req.userId, {
      relations: ["wallet"],
    });
    if (!user) return res.status(400).send("User not found");

    const item = await itemRepo.findOne(req.body.itemId);
    if (!item) return res.status(400).send("Item not found");

    let bal = user.wallet.balance - value;
    let st = item.stock--;

    if (bal < 0)
      return res.status(400).send("Usted no puede permitirse esta compra");

    if (st <= 0) return res.status(400).send("No existen unidades suficientes");

    await walletRepo.update(
      { id: user.wallet.id },
      {
        balance: bal,
      }
    );

    await itemRepo.update(
      { id: req.body.itemId },
      {
        stock: item.stock--,
      }
    );

    await generateTrans(
      "Compra de " + item.name,
      "Compra realizada del objeto " + item.name,
      value,
      user
    );

    return res.status(200).send("buy successfully");
  }
}

export default new PaymentController();
