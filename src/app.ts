import express, { Application } from "express";
import { createConnection } from "typeorm";
import { pagination } from "typeorm-pagination";
import authRoutes from "./routes/auth";
import itemRoutes from "./routes/item";
import paymentRoutes from "./routes/payment";
import morgan from "morgan";
import User from "./database/entities/user.entity";
import Item from "./database/entities/item.entity";
import cors from "cors";
import dotenv from "dotenv";
import Wallet from "./database/entities/wallet.entity";
import Transaction from "./database/entities/transaction.entity";
dotenv.config();

const app: Application = express();

// database options
createConnection({
  name: "default",
  type: "postgres",
  host: process.env.DATABASE_HOST || "",
  port: 5432,
  username: process.env.DATABASE_USERNAME || "",
  password: process.env.DATABASE_PASSWORD || "",
  database: process.env.DATABASE_NAME || "",
  entities: [User, Item, Wallet, Transaction],
  logging: true,
  synchronize: true,
})
  .then()
  .catch((e) => console.log(e));

app.use(pagination);
// settings
app.set("port", 8080);

// middlewares
app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

// routes
app.use("/api/auth", authRoutes);
app.use("/api/store", itemRoutes);
app.use("/api/payment", paymentRoutes);

export default app;
