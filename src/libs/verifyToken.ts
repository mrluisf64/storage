import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";

interface IPayload {
  id: string;
  iat: number;
  exp: number;
}

export const TokenValidation = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.header("Authorization");
  if (!token) return res.status(401).json("Access denied");

  const payload = jwt.verify(
    token,
    process.env.TOKEN_SECRET || "TOKENTEST"
  ) as IPayload;

  req.userId = payload.id;

  next();
};
