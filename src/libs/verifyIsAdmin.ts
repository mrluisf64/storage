import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import User from "../database/entities/user.entity";
import { RoleEnum } from "./enums/RoleEnum";

export const VerifyIsAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const repository = getRepository(User);

  const user = await repository.findOne(req.userId);

  if (!user) return res.status(401).json("Access denied");

  if (user.role === RoleEnum.ADMIN) return next();

  return res.status(401).json("Access denied");
};
