# Mini app de inventario administrativo

Hola, para iniciar esta aplicación en su sistema necesitan tener postgreSQL en su computador, y tienen que crear dos bases de datos, una para el desarrollo (dev) y otra para el testing.

Para esta aplicación he usado NodeJS + Express + Typescript del lado del backend. Mientras que del lado del frontend utilice ReactJS + Bootstrap + multiples librerias de ReactJS que me encontré.

### Configuración

Bien, primero necesitarás utilizar el comando para instalar todas las dependencias.

```
npm install
```

Después tendrás que hacer una copia del archivo **.env.example**, el cual tendrá que tener de nombre **.env**, ahora, dentro de esta ultimo archivo deberás de rellenar los datos requeridos (principalmente datos sobre tu base de datos)

```
cp .env.example .env
```

Ahora tendrás que hacer uso del siguiente comando para que se compile correctamente el backend.

```
npm run start
```

Y listo. En principio debería de funcionar cuando ejecutes el siguiente comando:

```
npm run dev
```

Ah, y para verificar los tests deberas de usar este comando

```
npm test
```

Ahora, para inicializar el frontend deberas de seguir las siguientes indicaciones:

1. Pasamos a la carpeta **frontend** (recuerda estar en la raiz del proyecto)

```
cd frontend
```

2. Dentro de la carpeta en particular tendrás que hacer el comando para instalar las depedencias.

```
npm install
```

3. Después tendrás que hacer una copia del archivo **.env.example**, el cual tendrá que tener de nombre **.env**.

```
cp .env.example .env
```

4. Después tendrás que ejecutar el comando **build**.

```
npm run build
```

5. Y por ultimo, podrás iniciar el servidor mediante el comando

```
npm start
```

¡Espero que sea de su agrado esta app!
