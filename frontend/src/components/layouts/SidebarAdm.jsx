import { Menu } from "antd";
import {
  UserOutlined,
  KeyOutlined,
  ShoppingOutlined,
  ShopOutlined,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";

const SidebarAdm = (props) => {
  let history = useHistory();

  const onLogout = () => {
    localStorage.clear();
    history.push("/");
  };

  const handleProfile = () => {
    history.push("/app/profile");
  };

  const handleStore = () => {
    history.push("/app/store");
  };

  const handleInventory = () => {
    history.push("/app/inventory");
  };

  return (
    <>
      <Menu mode="horizontal">
        <Menu.Item
          key="profile"
          icon={<UserOutlined />}
          onClick={handleProfile}
        >
          Perfil
        </Menu.Item>
        <Menu.Item
          key="store"
          icon={<ShoppingOutlined />}
          onClick={handleStore}
        >
          Tienda
        </Menu.Item>
        <Menu.Item
          key="inventory"
          icon={<ShopOutlined />}
          onClick={handleInventory}
        >
          Inventario
        </Menu.Item>
        <Menu.Item key="logout" onClick={onLogout} icon={<KeyOutlined />}>
          Cerrar Sesión
        </Menu.Item>
      </Menu>
    </>
  );
};

export default SidebarAdm;
