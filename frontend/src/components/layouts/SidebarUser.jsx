import { Menu } from "antd";
import { UserOutlined, KeyOutlined, ShoppingOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

const SidebarUser = (props) => {
  let history = useHistory();

  const onLogout = () => {
    localStorage.clear();
    history.push("/");
  };

  const handleProfile = () => {
    history.push("/app/profile");
  };

  const handleStore = () => {
    history.push("/app/store");
  };

  return (
    <>
      <Menu mode="horizontal">
        <Menu.Item
          key="profile"
          icon={<UserOutlined />}
          onClick={handleProfile}
        >
          Perfil
        </Menu.Item>
        <Menu.Item
          key="store"
          icon={<ShoppingOutlined />}
          onClick={handleStore}
        >
          Tienda
        </Menu.Item>
        <Menu.Item key="logout" onClick={onLogout} icon={<KeyOutlined />}>
          Cerrar Sesión
        </Menu.Item>
      </Menu>
    </>
  );
};

export default SidebarUser;
