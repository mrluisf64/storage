import React, { useEffect, useState } from "react";
import { getDataDecrypt } from "../../helpers/encrypt";
import SidebarAdm from "./SidebarAdm";
import SidebarUser from "./SidebarUser";

const SidebarMain = () => {
  const [profile, setProfile] = useState();

  useEffect(() => {
    const data = getDataDecrypt("profile");
    setProfile(data);
  }, []);

  return (
    <>
      {profile && profile.role === "user" && <SidebarUser user={profile} />}
      {profile && profile.role === "admin" && <SidebarAdm user={profile} />}
    </>
  );
};

export default SidebarMain;
