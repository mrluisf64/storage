import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import AdminRoute from "../helpers/adminRoute";
import PrivateRoute from "../helpers/privateRoute";
import App from "./App";
import Profile from "./profile/Profile";
import SidebarMain from "./layouts/SidebarMain";
import Store from "./storage/Store";
import Inventory from "./storage/Inventory";

const Main = () => {
  return (
    <Router>
      <NotificationContainer />
      <Switch>
        <Route path="/" exact>
          <App />
        </Route>
        <Route path="/app">
          <SidebarMain />
          <PrivateRoute path="/app/store" component={Store} />
          <PrivateRoute path="/app/profile" component={Profile} />
          <AdminRoute path="/app/inventory" component={Inventory} />
        </Route>
      </Switch>
    </Router>
  );
};

export default Main;
