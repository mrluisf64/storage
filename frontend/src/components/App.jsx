import { Row, Col } from "antd";
import Login from "./auth/Login";
import Register from "./auth/Register";

import "../assets/css/app.css";

function App() {
  return (
    <>
      <Row justify="center">
        <Col span={11}>
          <Login />
        </Col>
        <Col span={11}>
          <Register />
        </Col>
      </Row>
    </>
  );
}

export default App;
