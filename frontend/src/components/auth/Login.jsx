import { Card, Form, Input, Button } from "antd";
import { setDataDecrypt } from "../../helpers/encrypt";
import AuthService from "../../services/authService";
import { useHistory } from "react-router-dom";
import { sendNotification, sendNotifyError } from "../../helpers/notify";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Login = () => {
  let history = useHistory();

  const onFinish = (values) => {
    AuthService.login(values).then((response) => {
      const { data } = response;
      const { body, session } = data;

      setDataDecrypt("session", session);
      setDataDecrypt("profile", body);

      sendNotification(
        "success",
        "Usted ha iniciado sesión correctamente",
        "¡Solicitud Exitosa!"
      );

      history.push("/app/profile");
    });
  };

  const onFinishFailed = (errorInfo) => {
    sendNotifyError();
  };

  return (
    <Card title="Incie Sesión" style={{ marginTop: 25, marginRight: 5 }}>
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Nombre de Usuario"
          name="username"
          rules={[
            {
              required: true,
              message: "Por favor ingrese su nombre de usuario",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Contraseña"
          name="password"
          rules={[
            { required: true, message: "Por favor ingrese su contraseña" },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Iniciar
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default Login;
