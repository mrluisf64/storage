import React, { useState } from "react";
import { Card, Button, Modal, Col, Row, Input } from "antd";
import PaymentService from "../../services/paymentService";
import { useHistory } from "react-router-dom";
import { sendNotification, sendNotifyError } from "../../helpers/notify";

const Wallet = (props) => {
  const [modalRe, setModalRe] = useState(false);
  const [recharge, setRecharge] = useState(0);
  let history = useHistory();

  const toggleRecharge = () => {
    setModalRe(!modalRe);
  };

  const handleRecharge = (event) => {
    setRecharge(event.target.value);
  };

  const onRecharge = () => {
    PaymentService.recharge(recharge)
      .then((res) => {
        sendNotification(
          "success",
          "Usted ha recargado exitosamente",
          "Recarga realizada!"
        );
        history.go("/profile");
      })
      .catch((e) => {
        sendNotifyError();
      });
  };

  return (
    <>
      <Card title="Billetera" loading={props.loading}>
        <p>
          Saldo disponible:{" "}
          {props.wallet ? props.wallet.balance : "No disponible"}
        </p>
        <Button type="primary" onClick={toggleRecharge}>
          Recargar
        </Button>
        <Modal
          title="Recargar Saldo"
          visible={modalRe}
          footer={null}
          onCancel={toggleRecharge}
        >
          <Row gutter={8}>
            <Col span={20}>
              <Input
                size="large"
                placeholder="Inserte el monto a recargar"
                onChange={handleRecharge}
                type="number"
              />
            </Col>
            <Col span={4}>
              <Button type="primary" onClick={onRecharge}>
                Enviar
              </Button>
            </Col>
          </Row>
        </Modal>
      </Card>
    </>
  );
};

export default Wallet;
