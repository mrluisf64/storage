import React, { useEffect, useState } from "react";
import AuthService from "../../services/authService";
import { Card, Row, Table, Col } from "antd";
import Wallet from "./Wallet";
import { sendNotifyError } from "../../helpers/notify";

const Profile = () => {
  const [profile, setProfile] = useState();
  const [wallet, setWallet] = useState();
  const [trans, setTrans] = useState();
  const [loading, setLoading] = useState(true);

  const columnsTrans = [
    { title: "Titulo", dataIndex: "title", key: "title" },
    { title: "Descripción", dataIndex: "description", key: "description" },
    {
      title: "Monto",
      dataIndex: "mount",
      key: "mount",
      render: (text) => (text ? text : "No disponible"),
    },
    {
      title: "Fecha",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (date) => {
        const datetime = new Date(date);
        return datetime.toLocaleDateString("en-US");
      },
      sortDirections: ["descend"],
      sorter: (a, b) => {
        const dateA = new Date(a.createdAt);
        const dateB = new Date(b.createdAt);
        return dateA.valueOf() - dateB.valueOf();
      },
      defaultSortOrder: ["descend"],
    },
  ];

  useEffect(() => {
    AuthService.profile()
      .then((res) => {
        const { data } = res;
        setProfile(data);
        setWallet(data.wallet);
        setTrans(data.transactions);
        setLoading(false);
      })
      .catch((e) => {
        sendNotifyError();
      });
  }, []);

  return (
    <>
      <Row gutter={16}>
        <Col span={8}>
          <Card
            title="Perfil"
            style={{ marginTop: 17, marginBottom: 15 }}
            loading={loading}
          >
            {profile && (
              <>
                <p>Nombre: {profile.name}</p>
                <p>Nombre Clave: {profile.username}</p>
                <p>
                  Rol: {profile.role === "admin" ? "Administrador" : "Usuario"}
                </p>
              </>
            )}
            {!profile && "Datos no disponibles"}
          </Card>
          <Row gutter={16}>
            <Col span={24}>
              <Wallet loading={loading} wallet={wallet} />
            </Col>
          </Row>
        </Col>

        <Col span={16}>
          <Card
            title="Transacciones"
            style={{ marginTop: 17 }}
            loading={loading}
          >
            <Table
              columns={columnsTrans}
              dataSource={trans}
              pagination={{
                defaultPageSize: 5,
                showSizeChanger: true,
                pageSizeOptions: ["5", "8", "12"],
              }}
            />
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default Profile;
