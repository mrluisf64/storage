import { sendNotification, sendNotifyError } from "../../../helpers/notify";
import ItemService from "../../../services/itemService";
import { Modal, Input, Button, Form } from "antd";
import { useEffect } from "react";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const ModalUpdate = (props) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (props.item) {
      form.setFieldsValue({
        name: props.item.name,
        description: props.item.description,
        price: props.item.price,
        category: props.item.category,
        stock: props.item.stock,
      });
    }
  }, [form, props.item]);

  const onFinish = (values) => {
    const body = { ...values, itemId: props.item.id };

    ItemService.update(body)
      .then(() => {
        sendNotification(
          "success",
          "¡Objeto generado correctamente!",
          "¡Solicitud exitosa!"
        );
        props.toggleUpdate();
        props.updatedItems();
      })
      .catch((e) => {
        sendNotifyError();
      });
  };

  const onFinishFailed = () => {
    sendNotifyError();
  };

  return (
    <>
      {props.item && (
        <Modal
          title={"Modificar " + props.item.name}
          visible={props.modalUpdate}
          footer={null}
          onCancel={props.toggleUpdate}
        >
          <Form
            {...layout}
            form={form}
            name="basic"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Nombre"
              name="name"
              rules={[{ required: true, message: "Ingrese un nombre" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Descripción"
              name="description"
              rules={[
                {
                  required: true,
                  message: "Ingrese una descripción del objeto",
                },
              ]}
            >
              <Input type="text" />
            </Form.Item>

            <Form.Item
              label="Precio"
              name="price"
              rules={[{ required: true, message: "Ingrese un precio" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Categoría"
              name="category"
              rules={[{ required: true, message: "Ingrese una categoria" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Cantidad Disponible"
              name="stock"
              rules={[
                { required: true, message: "Ingrese la cantidad existente" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      )}
    </>
  );
};

export default ModalUpdate;
