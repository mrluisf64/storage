import { Modal, Input, Button, Form } from "antd";
import { sendNotification, sendNotifyError } from "../../../helpers/notify";
import ItemService from "../../../services/itemService";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const ModalCreate = (props) => {
  const onFinish = (values) => {
    ItemService.create(values)
      .then(() => {
        sendNotification(
          "success",
          "¡Objeto generado correctamente!",
          "¡Solicitud exitosa!"
        );
        props.toggleCreate();
        props.updatedItems();
      })
      .catch((e) => {
        sendNotifyError();
      });
  };

  const onFinishFailed = () => {
    sendNotifyError();
  };

  return (
    <Modal
      title="Crear Objeto"
      visible={props.modalCreate}
      footer={null}
      onCancel={props.toggleCreate}
    >
      <Form
        {...layout}
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Nombre"
          name="name"
          rules={[{ required: true, message: "Ingrese un nombre" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Descripción"
          name="description"
          rules={[
            { required: true, message: "Ingrese una descripción del objeto" },
          ]}
        >
          <Input type="text" />
        </Form.Item>

        <Form.Item
          label="Precio"
          name="price"
          rules={[{ required: true, message: "Ingrese un precio" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Categoría"
          name="category"
          rules={[{ required: true, message: "Ingrese una categoria" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Cantidad Disponible"
          name="stock"
          rules={[{ required: true, message: "Ingrese la cantidad existente" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Enviar
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalCreate;
