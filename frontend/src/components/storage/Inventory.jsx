import React, { useEffect, useState } from "react";
import { Card, Table, Button } from "antd";
import ItemService from "../../services/itemService";
import ModalCreate from "./modal/modalCreate";
import ModalUpdate from "./modal/modalUpdate";
import { sendNotification, sendNotifyError } from "../../helpers/notify";

const Inventory = () => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState();
  const [modalCreate, setModalCreate] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [itemSelected, setItemSelected] = useState();

  const columns = [
    { title: "Nombre", dataIndex: "name", key: "name" },
    { title: "Descripción", dataIndex: "description", key: "description" },
    { title: "Precio", dataIndex: "price", key: "price" },
    { title: "Categoría", dataIndex: "category", key: "category" },
    {
      title: "Cantidad",
      dataIndex: "stock",
      key: "stock",
      render: (text, record) =>
        record.stock < 0 ? "No hay unidades disponibles" : record.stock,
    },
    {
      title: "Fecha",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (date) => {
        const datetime = new Date(date);
        return datetime.toLocaleDateString("en-US");
      },
      sortDirections: ["descend"],
      sorter: (a, b) => {
        const dateA = new Date(a.createdAt);
        const dateB = new Date(b.createdAt);
        return dateA.valueOf() - dateB.valueOf();
      },
      defaultSortOrder: ["descend"],
    },
    {
      title: "Opciones",
      key: "options",
      render: (text, record) => (
        <>
          <Button onClick={() => onEdit(record)} type="primary">
            Editar
          </Button>
          <Button onClick={() => onDelete(record)} type="dashed">
            Eliminar
          </Button>
        </>
      ),
    },
  ];

  const onEdit = (row) => {
    setItemSelected(row);
    toggleUpdate();
  };

  const onDelete = (row) => {
    ItemService.delete(row.id)
      .then(() => {
        sendNotification(
          "success",
          "Se ha eliminado el objeto correctamente",
          "¡Objeto eliminado!"
        );
        const newList = items.filter((value) => value.id !== row.id);
        setItems(newList);
      })
      .catch(() => sendNotifyError());
  };

  const toggleCreate = () => {
    setModalCreate(!modalCreate);
  };

  const toggleUpdate = () => {
    setModalUpdate(!modalUpdate);
  };

  const updatedItems = () => {
    setLoading(true);
    ItemService.list().then((res) => {
      const { data } = res;
      setItems(data);
      setLoading(false);
    });
  };

  useEffect(() => {
    ItemService.list().then((res) => {
      const { data } = res;
      setItems(data);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <Card title="Inventario" style={{ marginTop: 17 }} loading={loading}>
        <Button type="default" onClick={toggleCreate}>
          Agregar Objeto
        </Button>
        <br />
        <Table
          columns={columns}
          dataSource={items}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: true,
            pageSizeOptions: ["10", "20", "30"],
          }}
        />
        <ModalCreate
          toggleCreate={toggleCreate}
          modalCreate={modalCreate}
          updatedItems={updatedItems}
        />
        <ModalUpdate
          toggleUpdate={toggleUpdate}
          modalUpdate={modalUpdate}
          updatedItems={updatedItems}
          item={itemSelected}
        />
      </Card>
    </>
  );
};

export default Inventory;
