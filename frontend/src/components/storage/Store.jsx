import React, { useEffect, useState } from "react";
import { Card, Table, Button } from "antd";
import ItemService from "../../services/itemService";
import PaymentService from "../../services/paymentService";
import { sendNotification } from "../../helpers/notify";

const Store = () => {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState();

  const columns = [
    { title: "Nombre", dataIndex: "name", key: "name" },
    { title: "Descripción", dataIndex: "description", key: "description" },
    { title: "Precio", dataIndex: "price", key: "price" },
    { title: "Categoría", dataIndex: "category", key: "category" },
    {
      title: "Fecha",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (date) => {
        const datetime = new Date(date);
        return datetime.toLocaleDateString("en-US");
      },
      sortDirections: ["descend"],
      sorter: (a, b) => {
        const dateA = new Date(a.createdAt);
        const dateB = new Date(b.createdAt);
        return dateA.valueOf() - dateB.valueOf();
      },
      defaultSortOrder: ["descend"],
    },
    {
      title: "Opciones",
      key: "options",
      render: (text, record) => (
        <Button onClick={() => onBuy(record)} type="primary">
          Comprar
        </Button>
      ),
    },
  ];

  const onBuy = (data) => {
    const body = {
      name: data.name,
      value: data.price,
      itemId: data.id,
    };

    PaymentService.buy(body)
      .then(() => {
        sendNotification(
          "success",
          "¡El objeto " + data.name + " ha sido comprado exitosamente!",
          "¡Solicitud exitosa!"
        );
      })
      .catch((e) => {
        sendNotification("error", e.response.data, "Hubo un problema");
      });
  };

  useEffect(() => {
    ItemService.list().then((res) => {
      const { data } = res;
      setItems(data);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <Card
        title="Tienda de objetos"
        style={{ marginTop: 17 }}
        loading={loading}
      >
        <Table
          columns={columns}
          dataSource={items}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: true,
            pageSizeOptions: ["10", "20", "30"],
          }}
        />
      </Card>
    </>
  );
};

export default Store;
