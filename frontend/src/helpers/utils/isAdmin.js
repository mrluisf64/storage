import { getDataDecrypt } from "../encrypt";

const isAdmin = () => {
  const profile = getDataDecrypt("profile");

  if (profile) {
    return profile.role === "admin" ? true : false;
  } else {
    return false;
  }
};

export default isAdmin;
