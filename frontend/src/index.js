import React from "react";
import ReactDOM from "react-dom";
import Main from "./components/Main";
import reportWebVitals from "./reportWebVitals";
import { getDataDecrypt } from "./helpers/encrypt";
import axios from "axios";
import "antd/dist/antd.css";
import "react-notifications/lib/notifications.css";

axios.defaults.baseURL = process.env.REACT_APP_API_URL + "api";

axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

axios.interceptors.request.use(
  async config => {
    if (localStorage.session) {
      config.headers = {
        Authorization: getDataDecrypt("session")
      };
    }
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

ReactDOM.render(
  <React.StrictMode>
    <Main />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
