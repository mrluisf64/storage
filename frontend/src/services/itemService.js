import axios from "axios";

class ItemService {
  list() {
    return axios.get("/store");
  }

  create(data) {
    return axios.post("/store", data);
  }

  update(data) {
    return axios.put("/store", data);
  }

  delete(id) {
    return axios.delete("/store/" + id);
  }
}

export default new ItemService();
