import axios from "axios";

class PaymentService {
  recharge(data) {
    return axios.post("/payment/recharge", {
      value: data,
    });
  }

  consult() {
    return axios.get("/payment/consult");
  }

  buy(data) {
    return axios.post("/payment/buy", data);
  }
}

export default new PaymentService();
