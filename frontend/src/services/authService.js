import axios from "axios";
class AuthService {
  login(data) {
    return axios.post("/auth/signin", data);
  }

  register(data) {
    return axios.post("/auth/signup", data);
  }

  profile(){
    return axios.get('/auth/profile');
  }

}

export default new AuthService();
